FROM python:3.7-slim-stretch

# install deps
WORKDIR /tmp
COPY ./poetry.lock .
COPY ./pyproject.toml .
RUN pip install poetry
RUN poetry config settings.virtualenvs.create false
RUN poetry install
RUN pip uninstall poetry -y

# copy in app
WORKDIR /app
COPY ./pieceofsh ./pieceofsh

# run with hypercorn (asgi server)
CMD hypercorn ./pieceofsh:prod_app\(\) --bind 0.0.0.0:80
