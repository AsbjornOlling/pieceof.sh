
import sys

sys.path.append('pieceofsh')


def test_subdomain_used():
    from pieceofsh.views import subdomain_used

    # no subdomains
    assert subdomain_used('pieceof.sh') is False
    assert subdomain_used('pieceof.sh:8888') is False
    assert subdomain_used('www.pieceof.sh') is False
    # (localhost)
    assert subdomain_used('localhost') is False
    assert subdomain_used('localhost:8888') is False
    assert subdomain_used('www.localhost') is False

    # lots of subdomains
    assert subdomain_used('subdom.pieceof.sh') is True
    assert subdomain_used('one.two.subdom.pieceof.sh') is True
    assert subdomain_used('subdom.pieceof.sh:8888') is True
    assert subdomain_used('42www--.pieceof.sh') is True
    # (localhost)
    assert subdomain_used('subdom.localhost') is True
    assert subdomain_used('one.two.subdom.localhost') is True
    assert subdomain_used('subdom.localhost:8888') is True
    assert subdomain_used('42www--.localhost') is True


def test_get_topic():
    from pieceofsh.views import get_topic

    assert get_topic('vim.pieceof.sh') == 'vim'
    assert get_topic('emacs.pieceof.sh') == 'emacs'
    assert get_topic('windows.pieceof.sh:8080') == 'windows'
    assert get_topic('urmom.localhost:5000') == 'urmom'
    assert get_topic('pineapple.on.pizza.pieceof.sh') == 'pineapple on pizza'
    assert get_topic('pineapple_on_pizza.pieceof.sh') == 'pineapple on pizza'
    assert get_topic('pineapple-on-pizza.pieceof.sh') == 'pineapple on pizza'
    assert get_topic('asbjørn.kjær.olling') == 'asbjørn kjær olling'
