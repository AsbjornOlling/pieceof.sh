
DOCKERTAG=pieceofsh
SRCDIR=./pieceofsh
CSSDIR=$(SRCDIR)/static/css
SCSSFILE=$(CSSDIR)/theme.scss
CSSFILE=$(CSSDIR)/style.css

run: css
	# only for development ofc
	# you mongoloid
	source .env && \
	poetry run \
	quart run -p 8080

test:
	python -m pytest

docker-build: css test
	docker build . -t $(DOCKERTAG)

docker-run: docker-build
	docker run -p 8080:80 $(DOCKERTAG) 

css:
	sass --sourcemap=none $(SCSSFILE):$(CSSFILE)
