# std lib
import re
import uuid

# deps
from loguru import logger
from quart import (
    Blueprint,
    request,
    render_template,
    redirect,
    url_for
)
import validators

# app imports
from pieceofsh import reasons

# flask blueprint
bp = Blueprint('views', __name__)


@bp.route('/')
async def index():
    """ Returns the topic-associated page if a subdomain is used
    otherwise show the generic default page.
    """
    if not subdomain_used(request.host):
        # if no subdomains used, return default page
        logger.info(f"Host: {request.host} - Returning default page.")
        return await render_template('default.j2.html')

    topic = get_topic(request.host)
    rs = [r async for r in reasons.get_reasons(topic)]
    return await render_template('topic.j2.html',
                                 topic=topic.upper(),
                                 reasons=rs)


@bp.route('/redirect', methods=('GET', 'POST'))
async def form_redirect():
    """ The endpoint used by the text field on the default page.
    Just redirects you to whatever topic you put in.
    """
    if request.method == 'GET':
        return redirect(url_for('views.index'))

    form = await request.form
    topic = form.get('topic')

    if topic is None or topic == '':
        return "You must provide a 'topic' to be redirected to.", 400

    return redirect(f'https://{topic}.pieceof.sh/')


@bp.route('/new_reason', methods=('POST',))
async def new_reason():
    form = await request.form
    text = form.get('text')
    if not text:
        return "You must provide a 'text'.", 400
    link = form.get('link', '')
    if link != '' and not validators.url(link):
        return f"{link!r} is not a valid url", 400
    await reasons.add_reason(get_topic(request.host), text, link)
    return redirect('/')


@bp.route('/vote/<reason_uuid>', methods=('POST',))
async def vote(reason_uuid):
    await reasons.vote_reason(get_topic(request.host), reason_uuid)
    return redirect('/')


def subdomain_used(hostheader: str) -> bool:
    """ True if any subdomain is used (apart from 'www.') """
    return bool(re.match(
        '^(?!www\\.).+\\.(pieceof\\.sh|localhost)(:[0-9]+)?$',
        hostheader
    ))


def get_topic(host: str) -> str:
    """ Strip 'pieceof.sh' from host header,
    and replace certain characters with spaces
    (and also e.g. localhost:5000 for local development)
    """
    # remove main domain from host header
    subdomain = re.sub('\\.(pieceof\\.sh|localhost)(:[0-9]+)*$', '',
                       host, flags=re.IGNORECASE)

    # decode idna encoding (for unicode in urls)
    if 'xn--' in subdomain:
        subdomain = bytes(subdomain, 'utf8').decode('idna')

    # '-', '_', and '.' become spaces
    topic = re.sub('[_\\-\\.]', ' ', subdomain)
    logger.debug(f"Host: {host} -> Topic: {topic}")
    return topic.lower()
