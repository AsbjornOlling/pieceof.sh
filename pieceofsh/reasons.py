"""
XXX: this file is not in use yet
its meant to deal with redis for storing user-uploaded reasons
"""

# std lib
from dataclasses import dataclass
from typing import Generator
from uuid import uuid4
import re

# deps
import aioredis
from loguru import logger
from quart import (
    g,
    abort,
    current_app as app
)

REDIS_PREFIX = 'pieceofsh:'
TOPICKEY_FORMAT = f'{REDIS_PREFIX}:{{topic}}'
REASONKEY_FORMAT = f'{TOPICKEY_FORMAT}:{{uuid}}'


@dataclass
class Reason:
    uuid: str
    votes: int
    text: str
    link: str


async def get_redis() -> aioredis.Redis:
    """ Returns async redis object.
    Cache the conenction object in Quart.g
    """
    if 'r' in g:
        return g.r

    r = await aioredis.create_redis(app.config['REDIS_ADDR'])
    g.r = r
    return r


async def get_reasons(topic: str) -> Generator[Reason, None, None]:
    """ Get reasons for a speficit topic from a Redis zset. """
    r = await get_redis()

    # get sorted list of reason keys
    zset = TOPICKEY_FORMAT.format(topic=topic)
    reasonkeys = await r.zrevrange(zset, 0, -1, withscores=True)

    # get each reason
    for key, votes in reasonkeys:
        rdata = await r.hmget(key, 'text', 'link')
        logger.debug(rdata)
        reason = Reason(key.decode('utf8').split(':')[-1], votes,
                        *[b.decode('utf8') for b in rdata])
        logger.info(f"Returning reason: {reason}")
        yield reason


async def add_reason(topic: str, text: str, link: str) -> None:
    """ Add a new Reason to Redis. """
    r = await get_redis()

    # put reason in redis with uuid key
    ruuid = uuid4()
    rkey = REASONKEY_FORMAT.format(topic=topic, uuid=ruuid)
    logger.info(f"Adding {rkey} to Redis.")
    await r.hmset(rkey, "text", text, "link", link)

    # put in in the redis zset with one vote
    await vote_reason(topic, ruuid)


async def vote_reason(topic: str, ruuid: str) -> None:
    """ Increase the votecount in Redis for a specific Reason. """
    # get topic zset key from rkey
    zset = TOPICKEY_FORMAT.format(topic=topic)
    rkey = REASONKEY_FORMAT.format(topic=topic, uuid=ruuid)

    logger.info(f"Voting on reason: {rkey} in zset: {zset}")
    r = await get_redis()
    await r.zincrby(zset, 1, rkey)
