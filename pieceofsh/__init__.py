from functools import partial
from os import getenv

from quart import Quart

__version__ = '0.1.0'


def create_app(config: dict):
    app = Quart(__name__,
                template_folder='./templates',
                static_folder='./static',
                static_url_path='/static')

    from . import views
    app.register_blueprint(views.bp)
    app.config.update(config)

    return app


prod_app = partial(create_app, config={'REDIS_ADDR': getenv('REDIS_ADDR')})
dev_app = partial(create_app, config={'REDIS_ADDR': 'redis://localhost'})
